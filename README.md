# Laravel - Create Authentication instead of using the default column name password


1. Create a new Provider by using this command `php artisan make:provider YourProviderAuth`
2. add this line
```
public function retrieveByCredentials(array $credentials)
{
    $query = $this->createModel()->newQuery();

    foreach ($credentials as $key => $value) {
        if (!Str::contains($key, 'Secret_Key')) {
            $query->where($key, $value);
        }
    }

    return $query->first();
}

/**
 * Validate a user against the given credentials.
 *
 * @param  UserContract  $user
 * @param  array  $credentials
 * @return bool
 */
public function validateCredentials(UserContract $user, array $credentials)
{
    $plain = $credentials['Secret_Key']; // Secret_Key -> is the column name and represent as password field
    return $this->hasher->check($plain, $user->getAuthPassword());
}
```

3. on the AuthServiceProvider.php in Provider folder add your YourProviderAuth on the boot function like this one
```
Auth::provider('ext_jwt', function ($app, $config) {
    return new DeskExtensionAuthProvider($this->app['hash'], $config['model']);
});
```

4. Edit the config > auth.php with your new provider
```
'ext_app' => [
    'driver' => 'ext_jwt',
    'model' => App\Customers::class,
    'table' => 'customers'
],
```
5. and you can also create a new guards
```
'api_ext' => [
    'driver' => 'jwt',
    'provider' => 'ext_app',
    'hash' => false,
],
```
6. edit you YourModel.php file add add this function
```
public function getAuthPassword()
{
    return $this->Secret_Key;
}
```
7. and now we're done.
